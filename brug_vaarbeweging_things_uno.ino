bool noLora = true; //dev true als er geen lora ontvangs is

/*------------------------------------------------------------------------------

  LIDARLite Arduino Library
  GetDistanceI2c

  This example shows how to initialize, configure, and read distance from a
  LIDAR-Lite connected over the I2C interface.

  Connections:
  LIDAR-Lite 5 Vdc (red) to Arduino 5v
  LIDAR-Lite I2C SCL (green) to Arduino SCL
  LIDAR-Lite I2C SDA (blue) to Arduino SDA
  LIDAR-Lite Ground (black) to Arduino GND

  (Capacitor recommended to mitigate inrush current when device is enabled)
  680uF capacitor (+) to Arduino 5v
  680uF capacitor (-) to Arduino GND

  See the Operation Manual for wiring diagrams and more information:
  http://static.garmin.com/pumac/LIDAR_Lite_v3_Operation_Manual_and_Technical_Specifications.pdf

  ------------------------------------------------------------------------------*/
#include <TheThingsNetwork.h>
#include <Wire.h>
#include <LIDARLite.h>
// tutorial: http://henrysbench.capnfatz.com/henrys-bench/arduino-sensors-and-input/arduino-tiny-rtc-d1307-tutorial/
//RTC lib https://www.elecrow.com/wiki/index.php?title=File:RTC.zip
#include "RTClib.h"

// Set your AppEUI and AppKey
const char *appEui = "70B3D57ED00078FF";
const char *appKey = "A2DA7CD76C979A062B38C74ED227337A";

#define loraSerial Serial1
#define debugSerial Serial

// Replace REPLACE_ME with TTN_FP_EU868 or TTN_FP_US915
#define freqPlan TTN_FP_EU868

TheThingsNetwork ttn(loraSerial, debugSerial, freqPlan);

//project variables
const int bridgePin =  1;
bool bridgeStatus = false; //true = open, false = closed
unsigned long bridgeTime = 0;

//lora send buffers
byte ship_left_payload_count;
byte ship_left_payload[32];

byte ship_right_payload_count;
byte ship_rigth_payload[32];

byte bridge_payload_count;
byte bridge_payload[32];

//https://learn.sparkfun.com/tutorials/lidar-lite-v3-hookup-guide#hardware-overview
LIDARLite LidarLite;
const char LidarLite_left_adres = 0x60;
const char LidarLite_right_adres = 0x62;
const char bridge_pin = 8;

const int lidar_relay = 4;
const int brug_contact = 12;

int LidarLite_left_max;
int LidarLite_right_max;

long sensorTriggerTime1 = 0;
bool triggerReset1 = false;

long sensorTriggerTime2 = 0;
bool triggerReset2 = false;

long triggerResetTime = 30000;
int triggerTrashold = 10;

RTC_DS1307 RTC;

void setup()
{

  Serial.begin(9600); // Initialize serial connection to display distance readings

  //setup pins
  pinMode(bridge_pin, INPUT_PULLUP);
  pinMode(lidar_relay, OUTPUT);
  pinMode(13, OUTPUT);

  digitalWrite(lidar_relay, HIGH);

  loraSerial.begin(57600);
  debugSerial.begin(9600);

  // Wait a maximum of 10s for Serial Monitor
  while (!debugSerial && millis() < 10000)
    ;

  //debugSerial.println("-- PERSONALIZE");
  //ttn.personalize(devAddr, nwkSKey, appSKey);

  debugSerial.println("-- STATUS");
  ttn.showStatus();

  debugSerial.println("-- JOIN");
  if (!noLora)
    ttn.join(appEui, appKey);

  ship_left_payload_count = 0;
  ship_right_payload_count = 0;
  bridge_payload_count = 0;

  Serial.println("--CONFIGURE LIDAR");
  LidarLite.begin(0, true); // Set configuration to default and I2C to 400 kHz
  LidarLite.configure(0); // Change this number to try out alternate configurations

  Serial.println("start change");

  byte error, address;
  Wire.beginTransmission(LidarLite_left_adres);
  error = Wire.endTransmission();

  if (error != 0) { //lidar adress is not set yet
    changeAddress(LidarLite_left_adres,  true, 0x62);
  }

  delay(1000);
  Serial.println("--TURN ON LIDAR 2");
  digitalWrite(lidar_relay, LOW);

  //set lidar 2 config
  LidarLite.begin(0, true); // Set configuration to default and I2C to 400 kHz
  LidarLite.configure(0); // Change this number to try out alternate configurations

  Serial.println("--CALIBRATE LIDAR");
  calibrateMaxDistance();

  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    //RTC.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
  }
  DateTime now = RTC.now();
  Serial.print("boot time");
  Serial.println(now.unixtime());
}

void loop()
{
  static int loopcounter = 0;
  //debugSerial.println("-- LOOP");
  //Take a measurement with receiver bias correction and print to serial terminal
  //Serial.print("lidar 1: ");
  //Serial.println(LidarLite.distance(true, LidarLite_left_adres));
  //Serial.print("lidar 2: ");
  //Serial.println(LidarLite.distance(true));

  //Serial.print("sensor 1 is ");
  if (checkSensor1()) { // is er een boot ?
    //Serial.println("aan");
    if (!triggerReset1 && !triggerReset2) { // staat geen trigger aan vanaf de andere sensor
      sensorTriggerTime1 = millis();
      triggerReset1 = true; //zet deze trigger richting actief
    }

    if (triggerReset2) { // als de trigger van de andere richting actief is
      triggerReset2 = false; // reset de trigger
      int triggerlength =  millis() - sensorTriggerTime2; //bereken de lengte in ms
      saveBoot(triggerlength, 2); // stuur de gegevens naar de save boot functie
    } else {
      //Serial.println();
    }

  } else {
    //Serial.println("uit");
  }
  //copy voor de 2e sensor zeldfe als hierboven maar dan andersom
  //Serial.print("sensor 2 is ");
  if (checkSensor2()) {
    //Serial.print("aan ");

    if (!triggerReset1 && !triggerReset2) {
      sensorTriggerTime2 = millis();
      triggerReset2 = true;
    }

    if (triggerReset1) {
      triggerReset1 = false;
      int triggerlength =  millis() - sensorTriggerTime1;
      saveBoot(triggerlength, 1);
    } else {
      // Serial.println();
    }

  } else {
    //Serial.println("uit");
  }


  //Serial.print("de brug is ");
  if (!checkBrug()) { //brugdek staat omlaag
    if ((millis() - bridgeTime) > 5000 ) {
      DateTime now = RTC.now();
      uint16_t timeStamp = now.minute() * 60 + now.second();

      bridgeTime = (millis() - bridgeTime) / 100 ;
      add_to_bridge_payload(bridgeTime, timeStamp);
      //send_bridge_payload();
      Serial.println("--BRUG GESLOTEN");
    }
    bridgeTime = millis();
  }
  //brug omhoog doe helemaal niks!


  //reset als het te lang duurt;
  int triggerlength1 =  millis() - sensorTriggerTime1;
  if (triggerlength1 > triggerResetTime && triggerReset1 ) {
    triggerReset1 = false;
    Serial.println();
    Serial.println("trigger 1 timeout");
  }

  delay(500);           // wait .5 seconds for next scan

  loopcounter++;

  if (loopcounter > 30) //60 iedere 0.5 sec komt neer op iedere 30 seconden
  {
    DateTime now = RTC.now();
    uint16_t timeStamp = now.minute() * 60 + now.second();

    //add_to_bridge_payload(50000, timeStamp);
    loopcounter = 0;
    sendAllData();

  }
}

void sendAllData() {
  send_bridge_payload();
  send_ship_left_payload();
  send_ship_right_payload();
}

// https://github.com/PulsedLight3D/LIDARLite_v2_Arduino_Library/blob/master/LIDARLite/LIDARLite.cpp#L555
unsigned char changeAddress(char newI2cAddress,  bool disablePrimaryAddress, char currentLidarLiteAddress) {
  //  Array to save the serial number
  unsigned char serialNumber[2];
  unsigned char newI2cAddressArray[1];

  //  Read two bytes from 0x96 to get the serial number
  LidarLite.read(0x96, 2, serialNumber, false, currentLidarLiteAddress);
  //  Write the low byte of the serial number to 0x18
  LidarLite.write(0x18, serialNumber[0], currentLidarLiteAddress);
  //  Write the high byte of the serial number of 0x19
  LidarLite.write(0x19, serialNumber[1], currentLidarLiteAddress);
  //  Write the new address to 0x1a
  LidarLite.write(0x1a, newI2cAddress, currentLidarLiteAddress);


  while (newI2cAddress != newI2cAddressArray[0]) {
    LidarLite.read(0x1a, 1, newI2cAddressArray, false, currentLidarLiteAddress);
  }
  Serial.print("address change complete!");
  //  Choose whether or not to use the default address of 0x62
  if (disablePrimaryAddress) {
    LidarLite.write(0x1e, 0x08, currentLidarLiteAddress);
  } else {
    LidarLite.write(0x1e, 0x00, currentLidarLiteAddress);
  }

  return newI2cAddress;
}


void calibrateMaxDistance() {

  int mesurements_left = 0;

  //calibrate lidar left with 3 mesurements
  for (int i = 0; i < 3; i++) {
    mesurements_left += LidarLite.distance(true, LidarLite_left_adres);
    delay(50);
  }

  LidarLite_left_max = mesurements_left / 3;

  int mesurements_right = 0;

  //calibrate lidar left with 3 mesurements
  for (int i = 0; i < 3; i++) {
    mesurements_right += LidarLite.distance(true);
    delay(50);
  }

  LidarLite_right_max = mesurements_right / 3;

  //print results
  Serial.print("calibrated Left ");
  Serial.println(LidarLite_left_max);
  Serial.print("calibrated right ");
  Serial.println(LidarLite_right_max);
}

bool lastVal1 = false; //de laaste value van de sensor
bool checkSensor1() {
  bool curVal = !(LidarLite.distance(true, LidarLite_left_adres)  > LidarLite_left_max + 10 || LidarLite.distance(true, LidarLite_left_adres)  < LidarLite_left_max - 10 ); //op dit moment tijdelijk een button moet lidar code worden
  if (!lastVal1 && curVal ) { //check of de laaste value uit was zoja zet deze aan als de sensor aan meet ( dit is om dubbel trigger te voorkomen, suur maar 1 keer true tot dat de sensor een keer false heeft gemeten.
    lastVal1 = true;
    return true;
  }
  if (lastVal1 && !curVal) { // als de laatse value aan was en de sensor is uit reset de last val naar false
    lastVal1 = false;
  }

  return false;
}

bool lastVal2 = false;  // zie checksensor1
bool checkSensor2() {
  bool curVal = !(LidarLite.distance(true)  > LidarLite_right_max + 10 || LidarLite.distance(true)  < LidarLite_right_max - 10 );
  if (!lastVal2 && curVal ) {
    lastVal2 = true;
    return true;
  }
  if (lastVal2 && !curVal) {
    lastVal2 = false;
  }

  return false;
}

//@TODO nog maken
bool checkBrug() {
  if (digitalRead(bridge_pin) == LOW) {
    digitalWrite(13, HIGH);
    return false;
  }
  digitalWrite(13, LOW);
  return true;
}

//@TODO aanpassen
void saveBoot(long tijd, int richting) {

  uint16_t contactTime = tijd / 100;

  DateTime now = RTC.now();
  uint16_t timeStamp = now.minute() * 60 + now.second();

  if (richting == 1) { //left
    add_to_ship_left_payload(contactTime, timeStamp);
  } else {
    add_to_ship_right_payload(contactTime, timeStamp);
  }

  Serial.print("boot voorbij met een tijd (ms) van ");
  Serial.print(tijd);
  Serial.print(" in richting ");
  Serial.println(richting);

  //  send_ship_left_payload();
  //send_ship_right_payload();

}


void add_to_bridge_payload(uint16_t contactTime, uint16_t timeStamp) {

  if (bridge_payload_count >= 32) { //override if is full
    bridge_payload_count = 0;
  }

  bridge_payload[0 + bridge_payload_count] = highByte(contactTime);
  bridge_payload[1 + bridge_payload_count] = lowByte(contactTime);
  bridge_payload[2 + bridge_payload_count] = highByte(timeStamp);
  bridge_payload[3 + bridge_payload_count] = lowByte(timeStamp);

  bridge_payload_count += 4;
}

void send_bridge_payload() {
  if (noLora)
    return;
  Serial.print("sending bridge data : ");
  Serial.print(bridge_payload_count);
  if ( bridge_payload_count !=  0) { //check if byte array contain somthing
    ttn_response_t result = ttn.sendBytes(bridge_payload, sizeof(bridge_payload), 5);
    if (result >= 1) { //succesfull transmission or receive
      memset(bridge_payload, 0, sizeof(bridge_payload)); //clear bridge buffer
      bridge_payload_count = 0; // reset bridge timer
      Serial.println("send OK");
    }
  }
}

void add_to_ship_left_payload(uint16_t contactTime, uint16_t timeStamp) {

  if (ship_left_payload_count >= 32) { //override if is full
    ship_left_payload_count = 0;
  }
  debugSerial.println("--adding ");
  ship_left_payload[0 + ship_left_payload_count] = highByte(contactTime);
  ship_left_payload[1 + ship_left_payload_count] = lowByte(contactTime);
  ship_left_payload[2 + ship_left_payload_count] = highByte(timeStamp);
  ship_left_payload[3 + ship_left_payload_count] = lowByte(timeStamp);

  ship_left_payload_count += 4;
}

void send_ship_left_payload() {
  if (noLora)
    return;

  debugSerial.println("--SENDING LEFT");
  if (ship_left_payload_count > 0) { //check if byte array contain somthing
    debugSerial.println("--payload is not 0");
    ttn_response_t result = ttn.sendBytes(ship_left_payload, sizeof(ship_left_payload), 1);

    if (result >= 1) { //succesfull transmission or receive
      memset(ship_left_payload, 0, sizeof(ship_left_payload)); //clear bridge buffer
      ship_left_payload_count = 0; // reset bridge timer
    }
  }
}

void add_to_ship_right_payload(uint16_t contactTime, uint16_t timeStamp) {

  if (ship_right_payload_count >= 32) { //override if is full
    ship_right_payload_count = 0;
  }

  ship_rigth_payload[0 + ship_right_payload_count] = highByte(contactTime);
  ship_rigth_payload[1 + ship_right_payload_count] = lowByte(contactTime);
  ship_rigth_payload[2 + ship_right_payload_count] = highByte(timeStamp);
  ship_rigth_payload[3 + ship_right_payload_count] = lowByte(timeStamp);

  ship_right_payload_count += 4;
}

void send_ship_right_payload() {
  if (noLora)
    return;

  debugSerial.println("--SENDING RIGHT");
  if (ship_right_payload_count > 0) { //check if byte array contain somthing
    ttn_response_t result = ttn.sendBytes(ship_rigth_payload, sizeof(ship_rigth_payload), 2);

    if (result >= 1) { //succesfull transmission or receive
      memset(ship_rigth_payload, 0, sizeof(ship_rigth_payload)); //clear bridge buffer
      ship_right_payload_count = 0; // reset bridge timer
    }
  }
}

